// import {MigrationInterface, QueryRunner} from "typeorm";

// export class InitDb1637090738082 implements MigrationInterface {
//     name = 'InitDb1637090738082'

//     public async up(queryRunner: QueryRunner): Promise<void> {
//         await queryRunner.query(`CREATE TABLE "category" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "nsleft" integer NOT NULL DEFAULT '1', "nsright" integer NOT NULL DEFAULT '2', "parentId" integer, CONSTRAINT "PK_9c4e4a89e3674fc9f382d733f03" PRIMARY KEY ("id"))`);
//         await queryRunner.query(`CREATE TABLE "company_contact" ("id" SERIAL NOT NULL, "phoneNumber" character varying NOT NULL, "email" character varying NOT NULL, "note" character varying NOT NULL, "countryCode" character varying NOT NULL, "position" character varying NOT NULL, "firstName" character varying NOT NULL, "lastName" character varying NOT NULL, "isActive" boolean NOT NULL DEFAULT '1', "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_83805404ba83a62bac85a4a479f" PRIMARY KEY ("id"))`);
//         await queryRunner.query(`CREATE TABLE "tag" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_8e4052373c579afc1471f526760" PRIMARY KEY ("id"))`);
//         await queryRunner.query(`CREATE TABLE "demand" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "text" character varying NOT NULL, "creatorId" integer NOT NULL, "categoryId" integer NOT NULL, "companyId" integer NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "REL_b7681ca594c8d62820ce564c51" UNIQUE ("categoryId"), CONSTRAINT "PK_2e27cd7b3d79c50d197cb0b3924" PRIMARY KEY ("id"))`);
//         await queryRunner.query(`CREATE TABLE "supply" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "text" character varying NOT NULL, "creatorId" integer NOT NULL, "companyId" integer NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "categoryId" integer, CONSTRAINT "REL_fa2214d0eb0d6194b03453a7a4" UNIQUE ("categoryId"), CONSTRAINT "PK_11dcdc2def0eb6d10ed3ae0180d" PRIMARY KEY ("id"))`);
//         await queryRunner.query(`CREATE TABLE "upvote" ("value" integer NOT NULL, "companyId" integer NOT NULL, CONSTRAINT "PK_b429a32bc9390954cb7f5a227d9" PRIMARY KEY ("companyId"))`);
//         await queryRunner.query(`CREATE TABLE "user" ("id" SERIAL NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
//         await queryRunner.query(`CREATE TABLE "company" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "cin" text, "vat" text, "isVisible" boolean NOT NULL DEFAULT '1', "website" text, "street" character varying NOT NULL, "city" character varying NOT NULL, "region" text, "zipCode" text, "orientationNumber" text, "countryCode" character varying NOT NULL, "points" integer NOT NULL DEFAULT '0', "creatorId" integer NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_a76c5cd486f7779bd9c319afd27" UNIQUE ("name"), CONSTRAINT "PK_056f7854a7afdba7cbd6d45fc20" PRIMARY KEY ("id"))`);
//         await queryRunner.query(`CREATE TABLE "demand_tags_tag" ("demandId" integer NOT NULL, "tagId" integer NOT NULL, CONSTRAINT "PK_049cb61c2217d438d4398a2d55d" PRIMARY KEY ("demandId", "tagId"))`);
//         await queryRunner.query(`CREATE INDEX "IDX_9af5e66a4603d08cafcd424678" ON "demand_tags_tag" ("demandId") `);
//         await queryRunner.query(`CREATE INDEX "IDX_fc2c6a759438e63cd6f75a8d45" ON "demand_tags_tag" ("tagId") `);
//         await queryRunner.query(`CREATE TABLE "supply_tags_tag" ("supplyId" integer NOT NULL, "tagId" integer NOT NULL, CONSTRAINT "PK_20fa4cc47646ff5dd3b399137f5" PRIMARY KEY ("supplyId", "tagId"))`);
//         await queryRunner.query(`CREATE INDEX "IDX_8c079de5d4711c2fd999334658" ON "supply_tags_tag" ("supplyId") `);
//         await queryRunner.query(`CREATE INDEX "IDX_27b0ede96cfc0551a312fa9d37" ON "supply_tags_tag" ("tagId") `);
//         await queryRunner.query(`CREATE TABLE "company_company_contacts_company_contact" ("companyId" integer NOT NULL, "companyContactId" integer NOT NULL, CONSTRAINT "PK_72331cad956e943d26bfd855526" PRIMARY KEY ("companyId", "companyContactId"))`);
//         await queryRunner.query(`CREATE INDEX "IDX_d055568bd6360e5c51934d1734" ON "company_company_contacts_company_contact" ("companyId") `);
//         await queryRunner.query(`CREATE INDEX "IDX_2328c5bfd7feafda675895abf0" ON "company_company_contacts_company_contact" ("companyContactId") `);
//         await queryRunner.query(`ALTER TABLE "category" ADD CONSTRAINT "FK_d5456fd7e4c4866fec8ada1fa10" FOREIGN KEY ("parentId") REFERENCES "category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
//         await queryRunner.query(`ALTER TABLE "demand" ADD CONSTRAINT "FK_b7681ca594c8d62820ce564c512" FOREIGN KEY ("categoryId") REFERENCES "category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
//         await queryRunner.query(`ALTER TABLE "demand" ADD CONSTRAINT "FK_f31b06b5bfa8bdf4127eede75bb" FOREIGN KEY ("creatorId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
//         await queryRunner.query(`ALTER TABLE "demand" ADD CONSTRAINT "FK_23968b16024ed94d68cd4ac058b" FOREIGN KEY ("companyId") REFERENCES "company"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
//         await queryRunner.query(`ALTER TABLE "supply" ADD CONSTRAINT "FK_fa2214d0eb0d6194b03453a7a4a" FOREIGN KEY ("categoryId") REFERENCES "category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
//         await queryRunner.query(`ALTER TABLE "supply" ADD CONSTRAINT "FK_82913fbe1ab9ae26d62c5ec2ef5" FOREIGN KEY ("creatorId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
//         await queryRunner.query(`ALTER TABLE "supply" ADD CONSTRAINT "FK_07da09e23558194d288086efb29" FOREIGN KEY ("companyId") REFERENCES "company"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
//         await queryRunner.query(`ALTER TABLE "upvote" ADD CONSTRAINT "FK_b429a32bc9390954cb7f5a227d9" FOREIGN KEY ("companyId") REFERENCES "company"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
//         await queryRunner.query(`ALTER TABLE "company" ADD CONSTRAINT "FK_60199845e913ff11df3c256d6be" FOREIGN KEY ("creatorId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
//         await queryRunner.query(`ALTER TABLE "demand_tags_tag" ADD CONSTRAINT "FK_9af5e66a4603d08cafcd4246785" FOREIGN KEY ("demandId") REFERENCES "demand"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
//         await queryRunner.query(`ALTER TABLE "demand_tags_tag" ADD CONSTRAINT "FK_fc2c6a759438e63cd6f75a8d458" FOREIGN KEY ("tagId") REFERENCES "tag"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
//         await queryRunner.query(`ALTER TABLE "supply_tags_tag" ADD CONSTRAINT "FK_8c079de5d4711c2fd9993346588" FOREIGN KEY ("supplyId") REFERENCES "supply"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
//         await queryRunner.query(`ALTER TABLE "supply_tags_tag" ADD CONSTRAINT "FK_27b0ede96cfc0551a312fa9d37c" FOREIGN KEY ("tagId") REFERENCES "tag"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
//         await queryRunner.query(`ALTER TABLE "company_company_contacts_company_contact" ADD CONSTRAINT "FK_d055568bd6360e5c51934d1734c" FOREIGN KEY ("companyId") REFERENCES "company"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
//         await queryRunner.query(`ALTER TABLE "company_company_contacts_company_contact" ADD CONSTRAINT "FK_2328c5bfd7feafda675895abf06" FOREIGN KEY ("companyContactId") REFERENCES "company_contact"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
//     }

//     public async down(queryRunner: QueryRunner): Promise<void> {
//         await queryRunner.query(`ALTER TABLE "company_company_contacts_company_contact" DROP CONSTRAINT "FK_2328c5bfd7feafda675895abf06"`);
//         await queryRunner.query(`ALTER TABLE "company_company_contacts_company_contact" DROP CONSTRAINT "FK_d055568bd6360e5c51934d1734c"`);
//         await queryRunner.query(`ALTER TABLE "supply_tags_tag" DROP CONSTRAINT "FK_27b0ede96cfc0551a312fa9d37c"`);
//         await queryRunner.query(`ALTER TABLE "supply_tags_tag" DROP CONSTRAINT "FK_8c079de5d4711c2fd9993346588"`);
//         await queryRunner.query(`ALTER TABLE "demand_tags_tag" DROP CONSTRAINT "FK_fc2c6a759438e63cd6f75a8d458"`);
//         await queryRunner.query(`ALTER TABLE "demand_tags_tag" DROP CONSTRAINT "FK_9af5e66a4603d08cafcd4246785"`);
//         await queryRunner.query(`ALTER TABLE "company" DROP CONSTRAINT "FK_60199845e913ff11df3c256d6be"`);
//         await queryRunner.query(`ALTER TABLE "upvote" DROP CONSTRAINT "FK_b429a32bc9390954cb7f5a227d9"`);
//         await queryRunner.query(`ALTER TABLE "supply" DROP CONSTRAINT "FK_07da09e23558194d288086efb29"`);
//         await queryRunner.query(`ALTER TABLE "supply" DROP CONSTRAINT "FK_82913fbe1ab9ae26d62c5ec2ef5"`);
//         await queryRunner.query(`ALTER TABLE "supply" DROP CONSTRAINT "FK_fa2214d0eb0d6194b03453a7a4a"`);
//         await queryRunner.query(`ALTER TABLE "demand" DROP CONSTRAINT "FK_23968b16024ed94d68cd4ac058b"`);
//         await queryRunner.query(`ALTER TABLE "demand" DROP CONSTRAINT "FK_f31b06b5bfa8bdf4127eede75bb"`);
//         await queryRunner.query(`ALTER TABLE "demand" DROP CONSTRAINT "FK_b7681ca594c8d62820ce564c512"`);
//         await queryRunner.query(`ALTER TABLE "category" DROP CONSTRAINT "FK_d5456fd7e4c4866fec8ada1fa10"`);
//         await queryRunner.query(`DROP INDEX "public"."IDX_2328c5bfd7feafda675895abf0"`);
//         await queryRunner.query(`DROP INDEX "public"."IDX_d055568bd6360e5c51934d1734"`);
//         await queryRunner.query(`DROP TABLE "company_company_contacts_company_contact"`);
//         await queryRunner.query(`DROP INDEX "public"."IDX_27b0ede96cfc0551a312fa9d37"`);
//         await queryRunner.query(`DROP INDEX "public"."IDX_8c079de5d4711c2fd999334658"`);
//         await queryRunner.query(`DROP TABLE "supply_tags_tag"`);
//         await queryRunner.query(`DROP INDEX "public"."IDX_fc2c6a759438e63cd6f75a8d45"`);
//         await queryRunner.query(`DROP INDEX "public"."IDX_9af5e66a4603d08cafcd424678"`);
//         await queryRunner.query(`DROP TABLE "demand_tags_tag"`);
//         await queryRunner.query(`DROP TABLE "company"`);
//         await queryRunner.query(`DROP TABLE "user"`);
//         await queryRunner.query(`DROP TABLE "upvote"`);
//         await queryRunner.query(`DROP TABLE "supply"`);
//         await queryRunner.query(`DROP TABLE "demand"`);
//         await queryRunner.query(`DROP TABLE "tag"`);
//         await queryRunner.query(`DROP TABLE "company_contact"`);
//         await queryRunner.query(`DROP TABLE "category"`);
//     }

// }
