import { getConnection } from 'typeorm';
import { User } from '../../entities';

export class UserMutationService {
  updatePassword = (userId: number, password: string) => {
    return User.update({ id: userId }, { password });
  };

  createUser = (email: string, password: string) => {
    // the same
    // User.create({}).save();
    return getConnection()
      .createQueryBuilder()
      .insert()
      .into(User)
      .values({ email, password })
      .returning('*')
      .execute();
  };
}
