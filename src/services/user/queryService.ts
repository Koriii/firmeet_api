import { User } from '../../entities';

export class UserQueryService {
  getUserById = (userId: number) => {
    return User.findOne(userId);
  };

  getUserByMail = (email: string) => {
    return User.findOne({ where: { email } });
  };
}
