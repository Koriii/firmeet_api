import { NewsletterMutationService } from './mutationService';
import { NewsletterQueryService } from './queryService';

export { NewsletterQueryService, NewsletterMutationService };
