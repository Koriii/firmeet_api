import { NewsletterSubscription } from '../../entities';

export class NewsletterQueryService {
  async getByEmail(email: string): Promise<NewsletterSubscription | null> {
    const response = await NewsletterSubscription.findOne({ where: { email } });
    return response ?? null;
  }
}
