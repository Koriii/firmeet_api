import { NewsletterSubscription } from '../../entities';
import { NewsletterSubscriptionInputModel } from '../../models/newsletter';

export class NewsletterMutationService {
  async create(
    input: NewsletterSubscriptionInputModel,
    userId: number,
  ): Promise<NewsletterSubscription> {
    const entity = { ...input, userId };

    return await NewsletterSubscription.create(entity).save();
  }
}
