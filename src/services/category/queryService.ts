import { getManager } from 'typeorm';
import { Category } from '../../entities';

export class CategoryQueriesService {
  async getCategories(): Promise<Category[]> {
    const manager = getManager();
    const trees = await manager.getTreeRepository(Category).findTrees();
    return trees;
  }
}
