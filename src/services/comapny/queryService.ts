import { Company } from '../../entities';

export class CompanyQueryService {
  getByCreatorId(creatorId: number): Promise<Company[] | undefined> {
    return Company.find({ where: { creatorId: creatorId } });
  }

  getById(id: number): Promise<Company | undefined> {
    return Company.findOne(id);
  }
}
