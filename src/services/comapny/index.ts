import { CompanyMutationService } from './mutationService';
import { CompanyQueryService } from './queryService';

export { CompanyMutationService, CompanyQueryService };
