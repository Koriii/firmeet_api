import { getConnection } from 'typeorm';
import { Company } from '../../entities';
import { CompanyInputModel } from '../../models/company/CompanyInputModel';

export class CompanyMutationService {
  create(input: CompanyInputModel, userId: number): Promise<Company> {
    const { id, ...createInput } = input;

    return Company.create({
      ...createInput,
      creatorId: userId,
    }).save();
  }

  async update(id: number, input: CompanyInputModel, userId: number): Promise<Company | null> {
    const result = await getConnection()
      .createQueryBuilder()
      .update(Company)
      .set({ ...input })
      .where('id = :id AND "creatorId" = :creatorId', { id, creatorId: userId })
      .returning('*')
      .execute();

    return result.raw[0];
  }

  async delete(id: number, userId: number): Promise<boolean> {
    const company = await Company.findOne(id);
    if (!company) {
      return false;
    }
    if (company.creatorId !== userId) {
      throw new Error('not authorized');
    }

    getConnection()
      .transaction(async (tm) => {
        await tm.getRepository(Company).delete({ id });
      })
      .then(() => {
        return true;
      })
      .catch((err) => {
        throw new Error(err);
      });

    return true;
  }
}
