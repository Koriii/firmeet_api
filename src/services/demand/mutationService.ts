import { Demand } from '../../entities';
import { DemandPostInputModel } from '../../models/demand';
import { getConnection } from 'typeorm';

import { CompanyQueryService } from '../comapny';
import { Api404Error, Api500Error, ApiAuthError } from '../../utils/errors/GenericErrors';

export class DemandMutationService {
  companyQueryService = new CompanyQueryService();

  async create(input: DemandPostInputModel, userId: number): Promise<Demand> {
    if (input.useCompanyAddress) {
      const company = await this.companyQueryService.getById(input.companyId);
      if (company) {
        input.city = company.city;
        input.countryCode = company.countryCode;
        input.district = company.district;
        input.street = company.street;
        input.streetNumber = company.streetNumber;
        input.postalCode = company.postalCode;
      }
    }

    const entity = {
      ...input,
      creatorId: userId,
      companyId: input.companyId,
      categoryId: input.categoryId,
    } as Demand;

    return await Demand.create(entity).save();
  }

  async update(id: number, input: DemandPostInputModel, userId: number): Promise<Demand[]> {
    const entity = { ...input } as Demand;

    const result = await getConnection()
      .createQueryBuilder()
      .update(Demand)
      .set(entity)
      .where('id = :id AND "creatorId" = :creatorId', { id, creatorId: userId })
      .returning('*')
      .execute();

    return result.raw;
  }

  async delete(id: number, userId: number) {
    const demand = await Demand.findOne(id);
    if (!demand) {
      throw new Api404Error(`Demand with id ${id} was not found.`);
    }
    if (demand.creatorId !== userId) {
      throw new ApiAuthError('Not authorized');
    }

    getConnection()
      .transaction(async (tm) => {
        await tm.getRepository(Demand).delete({ id });
      })
      .then(() => {
        return true;
      })
      .catch((err) => {
        throw new Api500Error(err);
      });

    return true;
  }
}
