import { DemandMutationService } from './mutationService';
import { DemandQueriesService } from './queryService';

export { DemandMutationService, DemandQueriesService };
