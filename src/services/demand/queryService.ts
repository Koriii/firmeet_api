import { Demand } from '../../entities';
import { DemandFilterInputModel, PaginatedDemandPostsModel } from '../../models/demand';
import { setFilterWhereClause } from '../../utils/helperFunctions';
import { getConnection } from 'typeorm';

export class DemandQueriesService {
  async getPost(id: number): Promise<Demand | undefined> {
    return await Demand.findOne(id, { relations: ['creator', 'company'] });
  }

  async getPosts(
    limit: number,
    cursor: string | null,
    filters: DemandFilterInputModel,
  ): Promise<PaginatedDemandPostsModel> {
    const realLimit = Math.min(50, limit);
    const realLimitPlusOne = realLimit + 1;

    const replacements: any[] = [realLimitPlusOne];

    if (cursor) {
      replacements.push(new Date(parseInt(cursor)));
    }

    let categoryIds = [];
    if (filters.categoryId) {
      const categoryResult = await getConnection().query(
        `
          WITH RECURSIVE c AS (
            SELECT ${filters.categoryId} AS id
            UNION ALL
            SELECT ca.id
            FROM "FIRMEET_DEV".public.category AS ca
            JOIN c ON c.id = ca."parentId"
          )
          SELECT id FROM c;
        `,
      );
      categoryIds = categoryResult.map((category: { id: number }) => category.id);
      delete filters.categoryId;
    }

    const whereClause = setFilterWhereClause('d', cursor, filters);

    // __DATA LOADER___
    const demands = await getConnection().query(
      `
        SELECT d.*
        FROM demand d
        ${whereClause}
        ${
          categoryIds.length
            ? `${!whereClause ? 'WHERE' : 'AND'} d."categoryId" IN (${categoryIds})`
            : ''
        }
        ORDER BY d."createdAt" DESC
        LIMIT $1
        `,
      replacements,
    );

    return { posts: demands.slice(0, realLimit), hasMore: demands.length === realLimitPlusOne };
  }
}
