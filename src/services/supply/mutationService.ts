import { Supply } from '../../entities';
import { SupplyPostInputModel } from '../../models/supply';
import { getConnection } from 'typeorm';

import { CompanyQueryService } from '../comapny';
import { Api404Error, Api500Error, ApiAuthError } from '../../utils/errors/GenericErrors';

export class SupplyMutationService {
  companyQueryService = new CompanyQueryService();

  async create(input: SupplyPostInputModel, userId: number): Promise<Supply> {
    if (input.useCompanyAddress) {
      const company = await this.companyQueryService.getById(input.companyId);
      if (company) {
        input.city = company.city;
        input.countryCode = company.countryCode;
        input.district = company.district;
        input.street = company.street;
        input.streetNumber = company.streetNumber;
        input.postalCode = company.postalCode;
      }
    }

    const entity = {
      ...input,
      creatorId: userId,
      companyId: input.companyId,
      categoryId: input.categoryId,
    } as Supply;

    return await Supply.create(entity).save();
  }

  async update(id: number, input: SupplyPostInputModel, userId: number): Promise<Supply[]> {
    const entity = { ...input };

    const result = await getConnection()
      .createQueryBuilder()
      .update(Supply)
      .set(entity)
      .where('id = :id AND "creatorId" = :creatorId', { id, creatorId: userId })
      .returning('*')
      .execute();

    return result.raw;
  }

  async delete(id: number, userId: number): Promise<boolean> {
    const supply = await Supply.findOne(id);
    if (!supply) {
      throw new Api404Error(`Supply with id ${id} was not found.`);
    }
    if (supply.creatorId !== userId) {
      throw new ApiAuthError('not authenticated');
    }

    getConnection()
      .transaction(async (tm) => {
        await tm.getRepository(Supply).delete({ id });
      })
      .then(() => {
        return true;
      })
      .catch((err) => {
        throw new Api500Error(err);
      });

    return true;
  }
}
