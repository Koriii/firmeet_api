import { SupplyMutationService } from './mutationService';
import { SupplyQueryService } from './queryService';

export { SupplyMutationService, SupplyQueryService };
