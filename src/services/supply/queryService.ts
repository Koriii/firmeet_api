import { Supply } from '../../entities';
import { SupplyFilterInputModel, PaginatedSupplyPostsModel } from '../../models/supply';
import { setFilterWhereClause } from '../../utils/helperFunctions';
import { getConnection } from 'typeorm';

export class SupplyQueryService {
  getPost(id: number): Promise<Supply | undefined> {
    return Supply.findOne(id, { relations: ['creator', 'company'] });
  }

  async getPosts(
    limit: number,
    cursor: string | null,
    filters: SupplyFilterInputModel,
  ): Promise<PaginatedSupplyPostsModel> {
    const realLimit = Math.min(50, limit);
    const realLimitPlusOne = realLimit + 1;

    const replacements: any[] = [realLimitPlusOne];

    if (cursor) {
      replacements.push(new Date(parseInt(cursor)));
    }

    let categoryIds = [];
    if (filters.categoryId) {
      const categoryResult = await getConnection().query(
        `
          WITH RECURSIVE c AS (
            SELECT ${filters.categoryId} AS id
            UNION ALL
            SELECT ca.id
            FROM "FIRMEET_DEV".public.category AS ca
            JOIN c ON c.id = ca."parentId"
          )
          SELECT id FROM c;
        `,
      );
      categoryIds = categoryResult.map((category: { id: number }) => category.id);
      delete filters.categoryId;
    }

    const whereClause = setFilterWhereClause('s', cursor, filters);

    // __DATA LOADER___
    const supply = await getConnection().query(
      `
        SELECT s.*
        FROM supply s
        ${whereClause}
        ${
          categoryIds.length
            ? `${!whereClause ? 'WHERE' : 'AND'} s."categoryId" IN (${categoryIds})`
            : ''
        }
        ORDER BY s."createdAt" DESC
        LIMIT $1
        `,
      replacements,
    );

    return { posts: supply.slice(0, realLimit), hasMore: supply.length === realLimitPlusOne };
  }
}
