import { InputType, Field } from 'type-graphql';

@InputType()
export class EmailPasswordInputModel {
  @Field()
  email: string;
  @Field()
  password: string;
}
