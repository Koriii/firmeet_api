import { InputType, Field } from 'type-graphql';

@InputType()
export class CompanyInputModel {
  @Field({ nullable: true })
  id?: number;
  @Field()
  name: string;
  @Field()
  cin: string;
  @Field()
  vat: string;
  @Field({ nullable: true })
  website: string;
  // address
  @Field()
  countryCode!: string;
  @Field()
  district!: string;
  @Field()
  city!: string;
  @Field()
  postalCode!: string;
  @Field()
  street!: string;
  @Field()
  streetNumber!: string;
  @Field()
  locality!: string;
}
