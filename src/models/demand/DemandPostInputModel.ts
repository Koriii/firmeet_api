import { Field, InputType } from 'type-graphql';

@InputType()
export class DemandPostInputModel {
  @Field()
  title: string;
  @Field()
  text: string;
  @Field()
  companyId: number;
  @Field()
  categoryId: number;
  @Field()
  duration: number;
  // address
  @Field()
  // @Field({ nullable: true })
  countryCode: string;
  @Field()
  // @Field({ nullable: true })
  district: string;
  @Field()
  // @Field({ nullable: true })
  city: string;
  @Field()
  // @Field({ nullable: true })
  postalCode: string;
  @Field()
  // @Field({ nullable: true })
  street: string;
  @Field()
  // @Field({ nullable: true })
  streetNumber: string;
  @Field()
  locality: string;
  @Field({ defaultValue: true })
  useCompanyAddress: boolean;
}
