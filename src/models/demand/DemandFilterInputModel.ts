import { InputType, Field } from 'type-graphql';

@InputType()
export class DemandFilterInputModel {
  @Field({ nullable: true })
  categoryId?: number;
  @Field({ nullable: true })
  countryCode?: string;
}
