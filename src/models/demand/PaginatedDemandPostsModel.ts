import { Demand } from '../../entities';
import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class PaginatedDemandPostsModel {
  @Field(() => [Demand])
  posts: Demand[];
  @Field()
  hasMore: boolean;
}
