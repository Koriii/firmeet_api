import { Supply } from '../../entities';
import { ObjectType, Field } from 'type-graphql';

@ObjectType()
export class PaginatedSupplyPostsModel {
  @Field(() => [Supply])
  posts: Supply[];
  @Field()
  hasMore: boolean;
}
