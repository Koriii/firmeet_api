import { Field, InputType } from 'type-graphql';

@InputType()
export class SupplyPostInputModel {
  @Field()
  title: string;
  @Field()
  text: string;
  @Field()
  companyId: number;
  @Field()
  categoryId: number;
  @Field()
  duration: number;
  // address
  // @Field({ nullable: true })
  @Field()
  countryCode: string;
  // @Field({ nullable: true })
  @Field()
  district: string;
  // @Field({ nullable: true })
  @Field()
  city: string;
  // @Field({ nullable: true })
  @Field()
  postalCode: string;
  // @Field({ nullable: true })
  @Field()
  street: string;
  // @Field({ nullable: true })
  @Field()
  streetNumber: string;
  // @Field({ nullable: true })
  @Field()
  locality: string;
  @Field({ defaultValue: true })
  useCompanyAddress: boolean;
}
