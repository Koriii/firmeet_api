import { Field, ObjectType } from 'type-graphql';

// not used yet
@ObjectType()
export class CategoryResponseModel {
  @Field()
  id!: number;
  @Field()
  name: string;
  @Field()
  children: CategoryResponseModel[];
}
