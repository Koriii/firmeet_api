import { InputType, Field } from 'type-graphql';

@InputType()
export class NewsletterSubscriptionInputModel {
  @Field()
  email: string;
  @Field({ nullable: true })
  period: number;
  @Field(() => [Number], { nullable: true })
  categoryIds: number[];
}
