import 'reflect-metadata';
import Redis from 'ioredis';
import session from 'express-session';
import connectRedis from 'connect-redis';
import { buildSchema } from 'type-graphql';
import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core';
import cors from 'cors';
import { createConnection } from 'typeorm';
import path from 'path';
import 'dotenv-safe/config';
import express from 'express';

import { MyContext } from './types';
import { COOKIE_NAME, __prod__ } from './constants';
import { ApolloServer } from 'apollo-server-express';
import { createUserLoader } from './utils/createUserLoader';
import { DemandMutations, DemandQueries } from './resolvers/demand';
import { UserMutations, UserQueries } from './resolvers/user';
import { createUpvoteLoader } from './utils/createUpvoteLoader';
import {
  Demand,
  User,
  Company,
  CompanyContact,
  Supply,
  Upvote,
  Tag,
  Category,
  NewsletterSubscription,
} from './entities';
import { SupplyMutations, SupplyQueries } from './resolvers/supply';
import { ComapnyMutations, CompanyQueries } from './resolvers/company';
import { CategoryQueries } from './resolvers/category';
import { NewsletterMutations } from './resolvers/newsletter';

// /tmp/redis
// to run redis: redis-server /usr/local/etc/redis.conf

// npx typeorm migration:generate -n InitDb
// npx typeorm migration:create -n MockPosts

const main = async () => {
  //const conn =
  await createConnection({
    // cmd line command: createdb lireddit2
    type: 'postgres',
    // database: 'lireddit2',
    // username: 'postgres',
    // password: 'postgres',
    url: process.env.DATABASE_URL,
    logging: true,
    synchronize: true, // delete on prod
    migrations: [path.join(__dirname, './migrations/*')],
    entities: [
      Demand,
      User,
      Company,
      CompanyContact,
      Supply,
      Upvote,
      Tag,
      Category,
      NewsletterSubscription,
    ],
  });
  // await conn.runMigrations();

  const app = express();

  const RedisStore = connectRedis(session);
  const redis = new Redis(process.env.REDIS_URL);

  app.set('trust proxy', 1);
  app.use(
    cors({
      origin: process.env.CORS_ORIGIN,
      credentials: true,
    }),
  );

  app.use(
    session({
      name: COOKIE_NAME,
      store: new RedisStore({
        client: redis,
        disableTouch: true,
      }),
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 365 * 10, // ten years
        httpOnly: true,
        path: '/',
        sameSite: 'lax', // csrf
        secure: __prod__, // cookie only works in https
        domain: __prod__ ? '.firmeet.com' : undefined, // replace firmeet with my domain (do not have it now)
      },
      saveUninitialized: false,
      secret: process.env.SESSION_SECRET,
      resave: false,
    }),
  );

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [
        DemandMutations,
        DemandQueries,
        UserMutations,
        UserQueries,
        SupplyMutations,
        SupplyQueries,
        CompanyQueries,
        ComapnyMutations,
        CategoryQueries,
        NewsletterMutations,
      ],
      validate: false,
    }),
    // enable default graphQL playground
    plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
    context: ({ req, res }): MyContext => ({
      req,
      res,
      redis,
      userLoader: createUserLoader(),
      upvoteLoader: createUpvoteLoader(),
    }),
  });

  await apolloServer.start();

  apolloServer.applyMiddleware({ app, cors: false });

  app.listen(parseInt(process.env.PORT), () => {
    console.log('server started on localhost:4000');
  });
};

main().catch((err) => {
  console.log(err);
});
