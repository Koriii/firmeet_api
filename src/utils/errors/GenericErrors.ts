import { BaseError } from './BaseError';
import { HttpStatusCode } from './constants';

export class Api404Error extends BaseError {
  constructor(
    name: string,
    statusCode = HttpStatusCode.NOT_FOUND,
    isOperational = true,
    description = `Not found`,
  ) {
    super(name, statusCode, isOperational, description);
  }
}

export class Api500Error extends BaseError {
  constructor(
    name: string,
    httpCode = HttpStatusCode.INTERNAL_SERVER,
    isOperational = true,
    description = 'Internal server error',
  ) {
    super(name, httpCode, isOperational, description);
  }
}

export class ApiAuthError extends BaseError {
  constructor(
    name: string,
    httpCode = HttpStatusCode.INTERNAL_SERVER,
    isOperational = true,
    description = 'Not authenticated',
  ) {
    super(name, httpCode, isOperational, description);
  }
}
