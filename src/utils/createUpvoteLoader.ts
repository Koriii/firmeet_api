import { Upvote } from '../entities/Upvote';
import DataLoader from 'dataloader';

// [{postId: 5, userId: 10}]
// [{postId: 5, userId: 10, value: 1}]
export const createUpvoteLoader = () =>
  new DataLoader<{ companyId: number }, Upvote | null>(async (keys) => {
    const upvotes = await Upvote.findByIds(keys as any);
    const updootIdsToUpdoot: Record<string, Upvote> = {};

    upvotes.forEach((upvote) => {
      updootIdsToUpdoot[`${upvote.company}`] = upvote;
    });

    return keys.map((key) => updootIdsToUpdoot[`${key.companyId}|${key.companyId}`]);
  });
