import nodemailer from 'nodemailer';

// async..await is not allowed in global scope, must use a wrapper
export const sendEmail = async (to: string, html: string) => {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  // let testAccount = await nodemailer.createTestAccount();
  // console.log('tetAccount', testAccount);

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: 'smtppro.zoho.eu',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: 'info@firmeet.com', // testAccount.user,
      pass: process.env.MAIL_SECRET, //'vgexf4XdbxL5', // testAccount.pass,
    },
  });

  // send mail with defined transport object
  const info = await transporter.sendMail({
    from: 'info@firmeet.com', // sender address
    to: to,
    subject: 'Change password', // Subject line
    //text, //: 'Hello world?', // plain text body
    html,
  });

  console.log('Message response: %s', info.response);
  console.log('Message sent: %s', info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
};
