import { DemandFilterInputModel } from '../models/demand';
import { SupplyFilterInputModel } from '../models/supply';

export const setFilterWhereClause = (
  alias: string,
  cursor?: string | null,
  filters?: DemandFilterInputModel | SupplyFilterInputModel,
): string => {
  let whereClause = '';
  const filtersAsAny = filters as any;

  if (cursor && !whereClause) {
    whereClause = `WHERE ${alias}."createdAt" < $2 `;
  }

  if (filters) {
    for (const [key, value] of Object.entries(filters)) {
      let toMatchValue = typeof value === 'string' ? `'${value.toLocaleUpperCase()}'` : value;

      if (filtersAsAny[key] && !whereClause) {
        whereClause = `WHERE ${alias}."${key}" = ${toMatchValue} `;
        continue;
      }
      if (filtersAsAny[key]) {
        whereClause += `AND ${alias}."${key}" = ${toMatchValue} `;
      }
    }
  }

  return whereClause;
};

export const randomizeString = (length = 7) => (Math.random() + 1).toString(36).substring(length);
