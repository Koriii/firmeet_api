import { NewsletterSubscription } from '../../entities/Newsletter';
import { NewsletterMutationService, NewsletterQueryService } from '../../services/newsletter';
import { MyContext } from '../../types';
import { Mutation, Arg, Ctx, Resolver } from 'type-graphql';
import { NewsletterSubscriptionInputModel } from '../../models/newsletter';

@Resolver(NewsletterSubscription)
export class NewsletterMutations {
  newsletterMutationService = new NewsletterMutationService();
  newsletterQueryService = new NewsletterQueryService();

  @Mutation(() => NewsletterSubscription)
  async submitNewsletterSubscription(
    @Arg('input') input: NewsletterSubscriptionInputModel,
    @Ctx() { req }: MyContext,
  ): Promise<NewsletterSubscription> {
    const newsletter = await this.newsletterQueryService.getByEmail(input.email);
    if (!newsletter) {
      return await this.newsletterMutationService.create(input, req.session.user?.id);
    }
    return newsletter;
  }
}
