import { Query, Resolver } from 'type-graphql';
// import cloneDeep from 'lodash.clonedeep';

import { Category } from '../../entities';
import { CategoryQueriesService } from '../../services/category';

// TODO: rewrite to model instead of entity
@Resolver(Category)
export class CategoryQueries {
  categoryService = new CategoryQueriesService();

  @Query(() => [Category])
  async categories(): Promise<Category[]> {
    const categories = await this.categoryService.getCategories();

    return categories;
  }
}
