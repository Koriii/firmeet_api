import { Supply } from '../../entities/Supply';
import { isAuth } from '../../middleware/isAuth';
import { MyContext } from '../../types';
import { Mutation, UseMiddleware, Arg, Ctx, Int, Resolver } from 'type-graphql';
import { SupplyPostInputModel } from '../../models/supply/SupplyPostInputModel';
import { SupplyMutationService } from '../../services/supply';

@Resolver(Supply)
export class SupplyMutations {
  supplyService = new SupplyMutationService();

  @Mutation(() => Supply)
  @UseMiddleware(isAuth)
  createSupplyPost(
    @Arg('input') input: SupplyPostInputModel,
    @Ctx() { req }: MyContext,
  ): Promise<Supply> {
    return this.supplyService.create(input, req.session.user?.id);
  }

  @Mutation(() => Supply, { nullable: true })
  @UseMiddleware(isAuth)
  async updateSupplyPost(
    @Arg('id', () => Int) id: number,
    @Arg('input') input: SupplyPostInputModel,
    @Ctx() { req }: MyContext,
  ): Promise<Supply | null> {
    const result = await this.supplyService.update(id, input, req.session.user?.id);

    return result[0];
  }

  @Mutation(() => Boolean)
  @UseMiddleware(isAuth)
  async deleteSupplyPost(
    @Arg('id', () => Int) id: number,
    @Ctx() { req }: MyContext,
  ): Promise<boolean> {
    return await this.supplyService.delete(id, req.session.user?.id);
  }
}
