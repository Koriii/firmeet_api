import { SupplyMutations } from './mutations';
import { SupplyQueries } from './queries';

export { SupplyQueries, SupplyMutations };
