import { Supply } from '../../entities/Supply';
import { Query, Arg, Int, Resolver, Ctx, FieldResolver, Root } from 'type-graphql';
import { User } from '../../entities/User';
import { MyContext } from '../../types';
import { SupplyQueryService } from '../../services/supply';
import { PaginatedSupplyPostsModel, SupplyFilterInputModel } from '../../models/supply';
import { randomizeString } from '../../utils/helperFunctions';

@Resolver(Supply)
export class SupplyQueries {
  supplyService = new SupplyQueryService();

  @FieldResolver(() => String)
  textSnippet(@Root() root: Supply) {
    return root.text.slice(0, 50);
  }

  @FieldResolver(() => User)
  creator(@Root() supply: Supply, @Ctx() { userLoader }: MyContext) {
    return userLoader.load(supply.creatorId);
  }

  @Query(() => PaginatedSupplyPostsModel)
  async supplyPosts(
    @Arg('limit', () => Int) limit: number,
    @Arg('cursor', () => String, { nullable: true }) cursor: string | null,
    @Arg('filters', { nullable: true }) filters: SupplyFilterInputModel,
  ): Promise<PaginatedSupplyPostsModel> {
    const result = await this.supplyService.getPosts(limit, cursor, filters);

    return {
      posts: result.posts,
      hasMore: result.hasMore,
    };
  }

  @Query(() => Supply, { nullable: true })
  supplyPost(
    @Arg('id', () => Int) id: number,
    @Arg('userId', () => Int, { nullable: true }) userId: number,
  ): Promise<Supply | undefined> {
    return this.supplyService.getPost(id).then((res) => {
      // randomize on api, so unathrorized user is not able to see confidential info
      if (!userId && res) {
        return {
          ...res,
          street: randomizeString(),
          city: randomizeString(),
          streetNumber: randomizeString(),
          postalCode: randomizeString(),
          district: randomizeString(),
          company: {
            ...res.company,
            cin: randomizeString(),
            city: randomizeString(),
            district: randomizeString(),
            locality: randomizeString(),
            postalCode: randomizeString(),
            name: randomizeString(9),
            website: randomizeString(),
            street: randomizeString(),
            streetNumber: randomizeString(1),
          },
        } as Supply;
      } else {
        return res;
      }
    });
  }
}
