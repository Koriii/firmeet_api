import { Upvote } from '../../entities/Upvote';
import { isAuth } from '../../middleware/isAuth';
import { MyContext } from '../../types';
import {
  Arg,
  Ctx,
  FieldResolver,
  Int,
  Mutation,
  Resolver,
  Root,
  UseMiddleware,
} from 'type-graphql';
import { getConnection } from 'typeorm';
import { Company } from '../../entities/Company';

@Resolver(Upvote)
export class UpvoteMutations {
  @FieldResolver(() => Int, { nullable: true })
  async voteStatus(@Root() comapny: Company, @Ctx() { upvoteLoader, req }: MyContext) {
    if (!req.session.user?.id) {
      return null;
    }

    const upvote = await upvoteLoader.load({
      companyId: comapny.id,
    });

    return upvote ? upvote.value : null;
  }

  // TODO: change to company where possible
  @Mutation(() => Boolean)
  @UseMiddleware(isAuth)
  async vote(
    @Arg('companyId', () => Int) companyId: number,
    @Arg('value', () => Int) value: number,
    @Ctx() { req }: MyContext,
  ) {
    const isUpvote = value !== -1;
    const realValue = isUpvote ? 1 : -1;
    const userId = req.session.user?.id;

    const upvote = await Upvote.findOne({ where: { companyId, userId } });

    // the user has voted on the post before
    // and they are changing their vote
    if (upvote && upvote.value !== realValue) {
      getConnection().transaction(async (tm) => {
        await tm.query(`
          UPDATE upvote
          SET value = ${realValue}
          WHERE "companyId" = ${companyId} AND "userId" = ${userId}
        `);
        await tm.query(`
          UPDATE user
          SET points = points + ${realValue * 2}
          WHERE id = ${companyId};
        `);
      });
    } else if (!upvote) {
      // has never voter before
      getConnection().transaction(async (tm) => {
        await tm.query(`
          INSERT INTO upvote ("userId", "companyId", value)
          VALUES (${userId}, ${companyId}, ${realValue});
        `);
        await tm.query(`    
          UPDATE user
          SET points = points + ${realValue}
          WHERE id = ${companyId};
        `);
      });
    }

    return true;
  }
}
