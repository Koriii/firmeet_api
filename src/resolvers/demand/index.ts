import { DemandMutations } from './mutations';
import { DemandQueries } from './queries';

export { DemandQueries, DemandMutations };
