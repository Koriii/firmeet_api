import { Query, Arg, Int, Resolver, Ctx, FieldResolver, Root } from 'type-graphql';

import { Demand } from '../../entities/Demand';
import { User } from '../../entities/User';
import { MyContext } from '../../types';
import { DemandQueriesService } from '../../services/demand';
import { DemandFilterInputModel, PaginatedDemandPostsModel } from '../../models/demand';
import { randomizeString } from '../../utils/helperFunctions';

@Resolver(Demand)
export class DemandQueries {
  demandService = new DemandQueriesService();

  @FieldResolver(() => String)
  textSnippet(@Root() root: Demand) {
    return root.text.slice(0, 50);
  }

  @FieldResolver(() => User)
  creator(@Root() demand: Demand, @Ctx() { userLoader }: MyContext) {
    return userLoader.load(demand.creatorId);
  }

  @Query(() => PaginatedDemandPostsModel)
  async demandPosts(
    @Arg('limit', () => Int) limit: number,
    @Arg('cursor', () => String, { nullable: true }) cursor: string | null,
    @Arg('filters', { nullable: true }) filters: DemandFilterInputModel,
  ): Promise<PaginatedDemandPostsModel> {
    const result = await this.demandService.getPosts(limit, cursor, filters);

    return {
      posts: result.posts,
      hasMore: result.hasMore,
    };
  }

  @Query(() => Demand, { nullable: true })
  demandPost(
    @Arg('id', () => Int) id: number,
    @Arg('userId', () => Int, { nullable: true }) userId: number,
  ): Promise<Demand | undefined> {
    return this.demandService.getPost(id).then((res) => {
      // randomize on api, so unathrorized user is not able to see confidential info
      if (!userId && res) {
        return {
          ...res,
          street: randomizeString(),
          city: randomizeString(),
          streetNumber: randomizeString(),
          postalCode: randomizeString(),
          district: randomizeString(),
          company: {
            ...res.company,
            cin: randomizeString(),
            city: randomizeString(),
            district: randomizeString(),
            locality: randomizeString(),
            postalCode: randomizeString(),
            name: randomizeString(9),
            website: randomizeString(),
            street: randomizeString(),
            streetNumber: randomizeString(1),
          },
        } as Demand;
      } else {
        return res;
      }
    });
  }
}
