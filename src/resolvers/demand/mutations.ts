import { Demand } from '../../entities/Demand';
import { isAuth } from '../../middleware/isAuth';
import { MyContext } from '../../types';
import { Mutation, UseMiddleware, Arg, Ctx, Int, Resolver } from 'type-graphql';
import { DemandPostInputModel } from '../../models/demand/DemandPostInputModel';
import { DemandMutationService } from '../../services/demand';

@Resolver(Demand)
export class DemandMutations {
  demandService = new DemandMutationService();

  @Mutation(() => Demand)
  @UseMiddleware(isAuth)
  createDemandPost(
    @Arg('input') input: DemandPostInputModel,
    @Ctx() { req }: MyContext,
  ): Promise<Demand> {
    return this.demandService.create(input, req.session.user?.id);
  }

  @Mutation(() => Demand, { nullable: true })
  @UseMiddleware(isAuth)
  async updateDemandPost(
    @Arg('id', () => Int) id: number,
    @Arg('input') input: DemandPostInputModel,
    @Ctx() { req }: MyContext,
  ): Promise<Demand | null> {
    const result = await this.demandService.update(id, input, req.session.user?.id);

    return result[0];
  }

  @Mutation(() => Boolean)
  @UseMiddleware(isAuth)
  async deleteDemandPost(
    @Arg('id', () => Int) id: number,
    @Ctx() { req }: MyContext,
  ): Promise<boolean> {
    return await this.demandService.delete(id, req.session.user?.id);
  }
}
