import { User } from '../../entities/User';
import { MyContext } from '../../types';
import { Ctx, FieldResolver, Query, Resolver, Root } from 'type-graphql';
import { UserQueryService } from '../../services/user';

@Resolver(User)
export class UserQueries {
  userService = new UserQueryService();

  @FieldResolver(() => String)
  email(@Root() user: User, @Ctx() { req }: MyContext) {
    // this is the current user and its ok to show them their own email
    if (req.session.user?.id === user.id) {
      return user.email;
    }
    // current user wants to see someone elses email
    return '';
  }

  @Query(() => User, { nullable: true })
  me(@Ctx() { req }: MyContext) {
    // not logged in
    if (!req.session.user?.id) {
      return null;
    }

    return this.userService.getUserById(req.session.user.id);
  }
}
