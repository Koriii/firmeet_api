import argon2 from 'argon2';
import { Mutation, Arg, Ctx, Resolver } from 'type-graphql';
import { v4 } from 'uuid';
import { FORGET_PASSWORD_PREFIX, COOKIE_NAME } from '../../constants';
import { User } from '../../entities/User';
import { MyContext } from '../../types';
import { sendEmail } from '../../utils/sendEmail';
import { UserMutationService, UserQueryService } from '../../services/user';
import { EmailPasswordInputModel, UserResponseModel } from '../../models/User';
import { getErrorMessage, validateRegister } from './validations';
import { Api500Error } from '../../utils/errors/GenericErrors';

@Resolver(User)
export class UserMutations {
  userMutationService = new UserMutationService();
  userQueryService = new UserQueryService();

  @Mutation(() => UserResponseModel)
  async changePassword(
    @Arg('token') token: string,
    @Arg('newPassword') newPassword: string,
    @Ctx() { redis, req }: MyContext,
  ): Promise<UserResponseModel> {
    if (newPassword.length <= 2) {
      return getErrorMessage('newPassword', 'Length mush be greater than 2');
    }

    const key = FORGET_PASSWORD_PREFIX + token;
    const userIdKey = await redis.get(key);

    if (!userIdKey) {
      return getErrorMessage('token', 'Token expired');
    }

    const userId = parseInt(userIdKey);
    const user = await this.userQueryService.getUserById(userId);

    if (!user) {
      return getErrorMessage('token', 'User no longer exists');
    }

    const hashedPassword = await argon2.hash(newPassword);
    await this.userMutationService.updatePassword(userId, hashedPassword);
    await redis.del(key);

    // login after password change
    req.session.user = { id: user.id };

    return { user };
  }

  @Mutation(() => Boolean)
  async forgotPassword(
    @Arg('email') email: string,
    @Arg('language') language: string,
    @Ctx() { redis }: MyContext,
  ) {
    const user = await this.userQueryService.getUserByMail(email);

    // the email is not in database
    if (!user) {
      // not tell that the mail does not exist
      return true;
    }

    const token = v4();

    await redis.set(FORGET_PASSWORD_PREFIX + token, user.id, 'ex', 1000 * 60 * 60 * 24); // one day

    const link = `
    <a href="${process.env.CORS_ORIGIN}/change-password/${token}">
      ${language === 'en-US' ? 'this link' : 'tento odkaz'}
    </a>`;

    await sendEmail(
      email,
      language === 'en-US'
        ? `
        <h1>Reset password</h1>
        <p>
          Please change your password by clicking on ${link}.
        </p>
      `
        : `<h1>Obnovit heslo</h1>
        <p>
          Heslo si změníte kliknutím na ${link}.
        </p>
      `,
    );

    return true;
  }

  @Mutation(() => UserResponseModel)
  async register(
    @Arg('options') options: EmailPasswordInputModel,
    @Ctx() { req }: MyContext,
  ): Promise<UserResponseModel> {
    const errors = validateRegister(options);

    if (errors) {
      return { errors };
    }

    let user;

    try {
      const hashedPassword = await argon2.hash(options.password);
      const result = await this.userMutationService.createUser(options.email, hashedPassword);
      user = result.raw[0];
    } catch (err) {
      if (err.code === '23505') {
        return getErrorMessage('email', `Email '${options.email}' already taken`);
      }
    }

    req.session.user = { id: user.id };

    return { user };
  }

  @Mutation(() => UserResponseModel)
  async login(
    @Arg('email') email: string,
    @Arg('password') password: string,
    @Ctx() { req }: MyContext,
  ): Promise<UserResponseModel> {
    const user = await this.userQueryService.getUserByMail(email);

    if (!user) {
      return getErrorMessage('email', 'Email does not exist');
    }

    const valid = await argon2.verify(user.password, password);

    if (!valid) {
      return getErrorMessage('password', 'Password is not correct');
    }

    // store user id session
    // this will set a cookie on the user
    // keep them logged in
    req.session.user = { id: user.id };

    return { user };
  }

  @Mutation(() => Boolean)
  logout(@Ctx() { req, res }: MyContext) {
    const result = new Promise((resolve) =>
      req.session.destroy((error: Error) => {
        // might be lower if we want to destroy it only if it succeedes
        res.clearCookie(COOKIE_NAME);

        if (error) {
          console.log(error);

          resolve(false);
          throw new Api500Error(error.message);
        }

        resolve(true);
      }),
    );

    return result;
  }
}
