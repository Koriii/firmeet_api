import { EmailPasswordInputModel } from '../../models/User';

export const validateRegister = (options: EmailPasswordInputModel) => {
  if (!options.email.includes('@')) {
    return [
      {
        field: 'email',
        message: 'Invalid email format',
      },
    ];
  }

  if (options.password.length <= 3) {
    return [
      {
        field: 'password',
        message: 'Length mush be greater than 3',
      },
    ];
  }

  // default if none found
  return null;
};

interface IGetErrorMessageResponse {
  errors: { field: string; message: string }[];
}

// TODO: maybe extend dynamically create more than one error
export const getErrorMessage = (field: string, message: string): IGetErrorMessageResponse => ({
  errors: [{ field, message }],
});
