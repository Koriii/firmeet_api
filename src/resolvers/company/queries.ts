import { MyContext } from '../../types';
import {
  Arg,
  Ctx,
  Field,
  FieldResolver,
  Float,
  ObjectType,
  Query,
  Resolver,
  Root,
} from 'type-graphql';
import { Company, User } from '../../entities';
import { CompanyQueryService } from '../../services/comapny';

@ObjectType()
class OptionValue {
  @Field()
  value: number;
  @Field()
  label: string;
}

@Resolver(Company)
export class CompanyQueries {
  companyService = new CompanyQueryService();

  @FieldResolver(() => User)
  creator(@Root() company: Company, @Ctx() { userLoader }: MyContext) {
    return userLoader.load(company.creatorId);
  }

  @Query(() => [OptionValue])
  async companyOptions(
    @Arg('creatorId', () => Float) creatorId: number,
  ): Promise<OptionValue[] | undefined> {
    const companies = await this.companyService.getByCreatorId(creatorId);
    return companies?.map((company) => ({ value: company.id, label: company.name }));
  }

  @Query(() => [Company])
  companies(@Arg('creatorId', () => Float) creatorId: number): Promise<Company[] | undefined> {
    return this.companyService.getByCreatorId(creatorId);
  }
}
