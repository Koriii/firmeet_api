import { ComapnyMutations } from './mutations';
import { CompanyQueries } from './queries';

export { ComapnyMutations, CompanyQueries };
