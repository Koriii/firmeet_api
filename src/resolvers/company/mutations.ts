import { CompanyMutationService } from '../../services/comapny';
import { Mutation, UseMiddleware, Arg, Ctx, Resolver, Int } from 'type-graphql';

import { Company } from '../../entities';
import { isAuth } from '../../middleware/isAuth';
import { MyContext } from '../../types';
import { CompanyInputModel } from '../../models/company/CompanyInputModel';

@Resolver(Company)
export class ComapnyMutations {
  companyService = new CompanyMutationService();

  @Mutation(() => Company)
  @UseMiddleware(isAuth)
  createCompany(
    @Arg('input') input: CompanyInputModel,
    @Ctx() { req }: MyContext,
  ): Promise<Company> {
    return this.companyService.create(input, req.session.user?.id);
  }

  @Mutation(() => Company)
  @UseMiddleware(isAuth)
  async updateCompany(
    @Arg('id', () => Int) id: number,
    @Arg('input') input: CompanyInputModel,
    @Ctx() { req }: MyContext,
  ): Promise<Company | null> {
    return this.companyService.update(id, input, req.session.user?.id);
  }

  @Mutation(() => Boolean)
  @UseMiddleware(isAuth)
  async deleteCompany(
    @Arg('id', () => Int) id: number,
    @Ctx() { req }: MyContext,
  ): Promise<boolean> {
    return await this.companyService.delete(id, req.session.user?.id);
  }
}
