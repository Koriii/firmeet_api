import { Field, Int, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { CompanyContact, Demand, Supply, Upvote, User } from '.';

@ObjectType()
@Entity()
export class Company extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id!: number; // string is also supported

  @Field()
  @Column({ unique: true })
  name!: string;

  @Field(() => String, { nullable: true })
  @Column({ type: 'text', nullable: true })
  cin: string;

  @Field(() => String, { nullable: true })
  @Column({ type: 'text', nullable: true })
  vat: string;

  @Field()
  @Column({ type: 'bool', default: 1 })
  isVisible!: boolean;

  @Field(() => String, { nullable: true })
  @Column({ type: 'text', nullable: true })
  website: string;

  // address
  @Field()
  @Column({ length: 2 })
  countryCode!: string;

  @Field()
  @Column()
  district!: string;

  @Field()
  @Column()
  city!: string;

  @Field()
  @Column()
  postalCode!: string;

  @Field()
  @Column()
  street!: string;

  @Field()
  @Column()
  streetNumber!: string;

  @Field()
  @Column()
  locality!: string;

  // vote
  @Field()
  @Column({ type: 'int', default: 0 })
  points!: number;

  @Field(() => Int, { nullable: true })
  voteStatus: number | null;

  // Foreign keys
  @Field()
  @Column()
  creatorId: number;

  @Field(() => User)
  @ManyToOne(() => User, (user) => user.companies)
  creator: User;

  @OneToMany(() => Demand, (demand) => demand.company)
  demands: Demand[];

  @OneToMany(() => Supply, (supply) => supply.company)
  supply: Supply[];

  @OneToMany(() => Upvote, (upvote) => upvote.company)
  upvote: Upvote[];

  @ManyToMany(() => CompanyContact)
  @JoinTable()
  companyContacts: CompanyContact[];

  @Field(() => String)
  @CreateDateColumn()
  createdAt: Date;

  @Field(() => String)
  @UpdateDateColumn()
  updatedAt: Date;
}
