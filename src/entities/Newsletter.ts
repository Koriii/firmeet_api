import { Field, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Category } from '.';

@ObjectType()
@Entity()
export class NewsletterSubscription extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id!: number; // string is also supported

  @Field()
  @Column({ unique: true })
  email!: string;

  @Field()
  @Column({ default: 0 })
  period!: number;

  @Field()
  @Column()
  userId!: number;

  @Field()
  @Column({ nullable: true })
  targetCategoryId: number;

  @ManyToMany(() => Category)
  @JoinTable()
  targetCategories: Category[];

  @Field(() => String)
  @UpdateDateColumn()
  updatedAt: Date;

  @Field(() => String)
  @CreateDateColumn()
  createdAt: Date;
}
