import { Field, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Company, User } from '.';
import { Category } from './Category';
import { Tag } from './Tag';

@ObjectType()
@Entity()
export class Supply extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id!: number; // string is also supported

  @Field()
  @Column()
  title!: string;

  @Field()
  @Column()
  text!: string;

  @Field()
  @Column()
  creatorId: number;

  @Field()
  @Column()
  categoryId: number;

  @Field()
  @Column({ default: 0 })
  duration!: number;

  // address
  @Field()
  @Column({ length: 2 })
  countryCode!: string;

  @Field()
  @Column()
  district!: string;

  @Field()
  @Column()
  city!: string;

  @Field()
  @Column()
  postalCode!: string;

  @Field()
  @Column()
  street!: string;

  @Field()
  @Column()
  streetNumber!: string;

  @Field()
  @Column()
  locality!: string;

  // fks
  @Field(() => Category)
  @ManyToOne(() => Category, (category) => category.supplies)
  category: Category;

  @Field(() => User)
  @ManyToOne(() => User, (user) => user.supplies)
  creator: User;

  @Field()
  @Column()
  companyId: number;

  @Field(() => Company)
  @ManyToOne(() => Company, (company) => company.id)
  company: Company;

  @ManyToMany(() => Tag)
  @JoinTable()
  tags: Tag[];

  @Field(() => String)
  @CreateDateColumn()
  createdAt: Date;

  @Field(() => String)
  @UpdateDateColumn()
  updatedAt: Date;

  // others
  @Field({ defaultValue: true })
  useCompanyAddress: boolean;
}
