import { Field, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@ObjectType()
@Entity()
export class CompanyContact extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id!: number; // string is also supported

  @Field(() => String, { nullable: true })
  @Column()
  phoneNumber: string;

  @Field()
  @Column()
  email!: string;

  @Field(() => String, { nullable: true })
  @Column()
  note: string;

  @Field()
  @Column()
  countryCode: string;

  @Field(() => String, { nullable: true })
  @Column()
  position: string;

  @Field()
  @Column()
  firstName: string;

  @Field()
  @Column()
  lastName: string;

  @Field()
  @Column({ type: 'bool', default: 1 })
  isActive: boolean;

  @Field(() => String)
  @CreateDateColumn()
  createdAt: Date;

  @Field(() => String)
  @UpdateDateColumn()
  updatedAt: Date;
}
