import { Field, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  Tree,
  TreeChildren,
  TreeParent,
} from 'typeorm';
import { Demand, Supply } from '.';

@ObjectType()
@Entity()
@Tree('nested-set')
export class Category extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id!: number; // string is also supported

  @Field()
  @Column()
  name!: string;

  @Field(() => [Category])
  @TreeChildren()
  children: Category[];

  @TreeParent()
  parent: Category;

  @OneToMany(() => Demand, (demand) => demand.category)
  demands: Supply[];

  @OneToMany(() => Supply, (supply) => supply.category)
  supplies: Supply[];

  @Field(() => String)
  @CreateDateColumn()
  createdAt: Date;
}
