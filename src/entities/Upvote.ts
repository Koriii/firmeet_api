import { Field, ObjectType } from 'type-graphql';
import { BaseEntity, Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { Company } from './Company';

// TODO: will someday work for comapnies
@ObjectType()
@Entity()
export class Upvote extends BaseEntity {
  @Field()
  @Column({ type: 'int' })
  value: number;

  @Field()
  @PrimaryColumn()
  companyId: number;

  @Field(() => Company)
  @ManyToOne(() => Company, (company) => company.upvote)
  company: Company;
}
