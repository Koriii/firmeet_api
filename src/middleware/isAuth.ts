import { MyContext } from '../types';
import { MiddlewareFn } from 'type-graphql';

export const isAuth: MiddlewareFn<MyContext> = ({ context }, next) => {
  if (!context.req.session.user?.id) {
    throw new Error('Not authenticated');
  }

  return next();
};
